#ifndef QUATERNION_SEQUENCE_H
#define	QUATERNION_SEQUENCE_H
#include "quaternion.h"
#include<vector>
#include<map>

class QuaternionSequence {
public:

	//typedef size_type long long
	class size_type {
	public:
		long long index;
		size_type(long long ind);
		size_type(const size_type& base);
		size_type(size_type&& base);
		bool operator==(size_type const &q) const;
		bool operator!=(size_type const &q) const;
		bool operator<(size_type const &q) const;
		bool operator>(size_type const &q) const;
		bool operator<=(size_type const &q) const;
		bool operator>=(size_type const &q) const;
	};
	QuaternionSequence();
	//const ref
	QuaternionSequence(std::map<QuaternionSequence::size_type,
		Quaternion> m);
	//usunac ten konstruktor	
	QuaternionSequence(std::map<QuaternionSequence::size_type,
		Quaternion>&& m);
	QuaternionSequence(std::vector<Quaternion> v);
	QuaternionSequence(const QuaternionSequence& base);
	QuaternionSequence(QuaternionSequence&& base);
	QuaternionSequence& operator=(const QuaternionSequence &base);
	QuaternionSequence& operator=(QuaternionSequence&& base);
	QuaternionSequence& operator+=(const QuaternionSequence &rhs);
	const QuaternionSequence operator+(const QuaternionSequence &other) const;
	QuaternionSequence& operator-=(const QuaternionSequence &rhs);
	const QuaternionSequence operator-(const QuaternionSequence &other) const;
	QuaternionSequence& operator*=(const QuaternionSequence &rhs);
	const QuaternionSequence operator*(const QuaternionSequence &other) const;
	QuaternionSequence& operator*=(const Quaternion &q);
	QuaternionSequence& operator*=(const double &a);
	const QuaternionSequence operator*(const Quaternion &other) const;
	const QuaternionSequence operator*(const double &a) const;
	bool operator==(const QuaternionSequence & dane) const;
	bool operator!=(const QuaternionSequence & dane) const;
	explicit operator bool() const;
	//SAFE BOOL IDIOM
	bool operator!() const;
	static int count();
	void insert(long long n, Quaternion q);
	Quaternion operator[](long long i);
private:
	static int numberOfActiveSequences;
	bool operator<(const QuaternionSequence & dane) const;
	bool operator>(const QuaternionSequence & dane) const;
	bool operator<=(const QuaternionSequence & dane) const;
	bool operator>=(const QuaternionSequence & dane) const;
	operator double() const;
	operator int() const;
	std::map<QuaternionSequence::size_type, Quaternion> mapa;
	friend ostream & operator<<(ostream &wyjscie, const QuaternionSequence &s);
};
QuaternionSequence operator*(const Quaternion& a, const QuaternionSequence& b);
QuaternionSequence operator*(const double& a, const QuaternionSequence& b);
#endif
