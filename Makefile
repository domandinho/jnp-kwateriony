COMPILER = g++ -std=c++11
all:
	$(COMPILER) -c quaternion_sequence.cc -o quaternion.o
	$(COMPILER) -c main.cpp -o main.o
	$(COMPILER) quaternion.o main.o -o test
	./test
clean:
	rm quaternion.o main.o
	rm ./test
