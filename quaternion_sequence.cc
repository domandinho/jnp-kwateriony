#include "quaternion.h"
#include "quaternion_sequence.h"
#include <iostream>
#include <utility>
#include <math.h>
#include <vector>
//TO TEZ MOZNA WYWALIC
#define mv std::move
using namespace std;
int QuaternionSequence::numberOfActiveSequences = 0;

double Quaternion::R() const {
	return IndexR;
}

double Quaternion::I() const {
	return IndexI;
}

double Quaternion::J() const {
	return IndexJ;
}

double Quaternion::K() const {
	return IndexK;
}

Quaternion::Quaternion() :
IndexR(0), IndexI(0), IndexJ(0), IndexK(0) {
}

/*
 * JEZELI DAMY TEN KONSTRUCTOR JAKO EXPLICIT TO
 * UNIEMOZLIWIMY TAKA ISTRUKCJE
 */
Quaternion::Quaternion(double r) :
IndexR(r), IndexI(0), IndexJ(0), IndexK(0) {
}

Quaternion::Quaternion(double re, double im) :
IndexR(re), IndexI(im), IndexJ(0), IndexK(0) {
}

Quaternion::Quaternion(double r, double i, double j, double k) :
IndexR(r), IndexI(i), IndexJ(j), IndexK(k) {
}

Quaternion::Quaternion(const Quaternion& base) :
IndexR(base.IndexR), IndexI(base.IndexI),
IndexJ(base.IndexJ), IndexK(base.IndexK) {

}

Quaternion::Quaternion(Quaternion&& base) :
IndexR(mv(base.IndexR)),
IndexI(mv(base.IndexI)),
IndexJ(mv(base.IndexJ)),
IndexK(mv(base.IndexK)) {
	base.IndexI = 0;
	base.IndexJ = 0;
	base.IndexK = 0;
	base.IndexR = 0;
}

Quaternion conj(Quaternion base) {
	return Quaternion(base.R(), -base.I(),
		-base.J(), -base.K());
}

Quaternion Quaternion::conj() const {
	return Quaternion(IndexR, -IndexI,
		-IndexJ, -IndexK);
}

double Quaternion::norm() const {
	return sqrt(
		IndexR * IndexR +
		IndexI * IndexI +
		IndexJ * IndexJ +
		IndexK * IndexK
		);
}

double norm(Quaternion base) {
	return sqrt(
		base.R() * base.R() +
		base.I() * base.I() +
		base.J() * base.J() +
		base.K() * base.K()
		);

}

bool Quaternion::operator==(const Quaternion& dane) const {
	return (
		(dane.R() == IndexR) &&
		(dane.I() == IndexI) &&
		(dane.J() == IndexJ) &&
		(dane.K() == IndexK)
		);
}

bool Quaternion::operator==(const double& dane) const {
	return (
		(dane == IndexR) &&
		(0 == IndexI) &&
		(0 == IndexJ) &&
		(0 == IndexK)
		);
}

bool Quaternion::operator!=(const double& dane) const {
	return !(*this == dane);
}

bool Quaternion::operator!=(const Quaternion& dane) const {
	return !(*this == dane);
}

bool operator==(const double& a, const Quaternion& b) {
	return (b == a);
}
bool operator!=(const double& a, const Quaternion& b) {
	return (b != a);
}

Quaternion& Quaternion::operator=(const Quaternion& base) {
	if (this != &base) {
		IndexR = base.IndexR;
		IndexI = base.IndexI;
		IndexJ = base.IndexJ;
		IndexK = base.IndexK;
	}
	return *this;
}

Quaternion& Quaternion::operator=(Quaternion&& base) {
	if (&base != this) {
		IndexR = mv(base.IndexR);
		IndexI = mv(base.IndexI);
		IndexJ = mv(base.IndexJ);
		IndexK = mv(base.IndexK);
	}
	return *this;
}

Quaternion Quaternion::operator-() const {
	return Quaternion(-IndexR, -IndexI, -IndexJ, -IndexK);
}

Quaternion Quaternion::operator+() const {
	return Quaternion(*this);
}

Quaternion& Quaternion::operator+=(const Quaternion& rhs) {
	IndexR += rhs.IndexR;
	IndexI += rhs.IndexI;
	IndexJ += rhs.IndexJ;
	IndexK += rhs.IndexK;
	return *this;
}

const Quaternion Quaternion::operator+(const Quaternion& other) const {
	Quaternion result = *this;
	result += other;
	return result;
}

Quaternion& Quaternion::operator-=(const Quaternion& rhs) {
	IndexR -= rhs.IndexR;
	IndexI -= rhs.IndexI;
	IndexJ -= rhs.IndexJ;
	IndexK -= rhs.IndexK;
	return *this;
}

const Quaternion Quaternion::operator-(const Quaternion& other) const {
	Quaternion result = *this;
	result -= other;
	return result;
}

Quaternion::operator bool() const {
	return (
		(IndexR != 0) ||
		(IndexI != 0) ||
		(IndexJ != 0) ||
		(IndexK != 0)
		);
}

bool Quaternion::operator!() const {
	return (!(*this));
}

void printElement(bool& first, ostream& output, const double& param
	, string wsp) {
	if (param) {
		if (first) {
			if (param == -1 && wsp != "")
				output << "-";
			else if (wsp == "" || param != 1)
				output << param;
			output << wsp;
		} else {
			if (param >= 0) {
				output << "+";
			}
			if (param == -1)
				output << "-";
			else if (param != 1)
				output << param;
			output << wsp;
		}
		first = false;
	}
}

ostream& operator<<(ostream& wyjscie, const Quaternion& s) {
	bool first = true;
	if (s) {
		printElement(first, wyjscie, s.IndexR, "");
		printElement(first, wyjscie, s.IndexI, "i");
		printElement(first, wyjscie, s.IndexJ, "j");
		printElement(first, wyjscie, s.IndexK, "k");
	} else {
		wyjscie << "0";
	}
	wyjscie << "";
}

Quaternion& Quaternion::operator*=(const Quaternion& q) {
	double newR = IndexR;
	double newI = IndexI;
	double newJ = IndexJ;
	double newK = IndexK;
	IndexR = newR * q.IndexR - newK * q.IndexK - newJ * q.IndexJ
		- newI * q.IndexI;
	IndexI = newR * q.IndexI + newI * q.IndexR + newJ * q.IndexK
		- newK * q.IndexJ;
	IndexJ = newR * q.IndexJ - newI * q.IndexK + newJ * q.IndexR
		+ newK * q.IndexI;
	IndexK = newR * q.IndexK + newI * q.IndexJ - newJ * q.IndexI
		+ newK * q.IndexR;
	return *this;
}

const Quaternion Quaternion::operator*(const Quaternion& other) const {
	Quaternion result = *this;
	result *= other;
	return result;
}

const Quaternion Quaternion::operator*(const double &other) const {
	Quaternion result = *this;
	result *= other;
	return result;
}

Quaternion& Quaternion::operator*=(const double &q) {
	IndexR *= q;
	IndexI *= q;
	IndexJ *= q;
	IndexK *= q;
	return *this;
}

Quaternion operator*(const double& a, const Quaternion& b) {
	return b * a;
}

//DLA DODAWANIA I ODEJMOWANIA
const Quaternion Quaternion::operator+(const double &other) const {
	Quaternion result = *this;
	result += other;
	return result;
}

Quaternion& Quaternion::operator+=(const double &q) {
	IndexR += q;
}

Quaternion operator+(const double& a, const Quaternion& b) {
	return b + a;
}

const Quaternion Quaternion::operator-(const double &other) const {
	Quaternion result = *this;
	result -= other;
	return result;
}

Quaternion& Quaternion::operator-=(const double &q) {
	IndexR -= q;
}

Quaternion operator-(const double& a, const Quaternion& b) {
	return - (b - a);
}

QuaternionSequence& QuaternionSequence::operator*=(const Quaternion &q) {
	for (auto it = mapa.begin(); it != mapa.end(); it++) {
		it->second *= q;
	}
	return *this;
}

QuaternionSequence::QuaternionSequence() {
	numberOfActiveSequences++;
}

QuaternionSequence::size_type::size_type(long long ind) {
	index = ind;
}

QuaternionSequence::QuaternionSequence(std::map<
	QuaternionSequence::size_type, Quaternion> m) : mapa(m) {
	numberOfActiveSequences++;
}

QuaternionSequence::QuaternionSequence(std::map<
	QuaternionSequence::size_type, Quaternion>&& m) : mapa(m) {
	numberOfActiveSequences++;
}

QuaternionSequence::QuaternionSequence(std::vector<Quaternion> v) {
	numberOfActiveSequences++;
	int index = 0;
	for (auto it = v.begin(); it != v.end(); it++) {
		mapa.insert(make_pair(index, v[index]));
		index++;
	}
}

QuaternionSequence::size_type::size_type(const size_type& base) : index(base.index) {
}

QuaternionSequence::size_type::size_type(size_type&& base) : index(std::move(base.index)) {
}

bool QuaternionSequence::size_type::operator==(size_type const &q) const {
	return index == q.index;
}

bool QuaternionSequence::size_type::operator!=(size_type const &q) const {
	return index != q.index;
}

bool QuaternionSequence::size_type::operator<(size_type const &q) const {
	return index < q.index;
}

bool QuaternionSequence::size_type::operator>(size_type const &q) const {
	return index > q.index;
};

bool QuaternionSequence::size_type::operator<=(size_type const &q) const {
	return !(index > q.index);
}

bool QuaternionSequence::size_type::operator>=(size_type const &q) const {
	return !(index < q.index);
};

QuaternionSequence::QuaternionSequence
(const QuaternionSequence& base) : mapa(base.mapa) {
	numberOfActiveSequences++;
}

QuaternionSequence::QuaternionSequence(QuaternionSequence&& base)
: mapa(std::move(base.mapa)) {
	numberOfActiveSequences++;
}

QuaternionSequence& QuaternionSequence::operator=(
	const QuaternionSequence& base) {
	mapa = base.mapa;
	return *this;
}

QuaternionSequence& QuaternionSequence::operator=(
	QuaternionSequence&& base) {
	mapa = std::move(base.mapa);
	return *this;
}

//LEPIEJ TO SPRAWDZ

QuaternionSequence& QuaternionSequence::operator
	+=(const QuaternionSequence& rhs) {
	map<QuaternionSequence::size_type, Quaternion> buffer;
	for (auto it = rhs.mapa.begin(); it != rhs.mapa.end(); it++) {
		if (mapa.find(it->first.index) != mapa.end()) {
			mapa[it->first] += it->second;
		} else {
			buffer.insert(make_pair(it->first, it->second));
		}
	}
	mapa.insert(buffer.begin(), buffer.end());
	return *this;
}

const QuaternionSequence QuaternionSequence::operator+
(const QuaternionSequence& other) const {
	QuaternionSequence next = *this;
	next += other;
	return next;
}

QuaternionSequence& QuaternionSequence::operator
	-=(const QuaternionSequence& rhs) {
	map<QuaternionSequence::size_type, Quaternion> buffer;
	for (auto it = rhs.mapa.begin(); it != rhs.mapa.end(); it++) {
		if (mapa.find(it->first.index) != mapa.end()) {
			mapa[it->first] -= it->second;
		} else {
			buffer.insert(make_pair(it->first, -(it->second)));
		}
	}
	mapa.insert(buffer.begin(), buffer.end());
	return *this;
}

const QuaternionSequence QuaternionSequence::operator-
(const QuaternionSequence& other) const {
	QuaternionSequence next = *this;
	next -= other;
	return next;
}

QuaternionSequence& QuaternionSequence::operator
	*=(const QuaternionSequence& rhs) {
	map<QuaternionSequence::size_type, Quaternion> buffer;
	for (auto it = mapa.begin(); it != mapa.end(); it++)
		if (rhs.mapa.find(it->first.index) != rhs.mapa.end())
			it->second *= rhs.mapa.find(it->first.index)->second;
		else
			buffer.insert(make_pair(it->first, it->second));
	map<QuaternionSequence::size_type, Quaternion>::iterator a;
	for (auto it = buffer.begin(); it != buffer.end(); it++) {
		a = mapa.find(it->first.index);
		mapa.erase(a);
	}
	return *this;
}

const QuaternionSequence QuaternionSequence::operator*
(const QuaternionSequence& other) const {
	QuaternionSequence next = *this;
	next *= other;
	return next;
}

ostream& operator<<(ostream& wyjscie, const QuaternionSequence& s) {
	wyjscie << "(";
	unsigned int ileNiezerowych = 0;
	for (auto it = s.mapa.begin(); it != s.mapa.end(); it++) {
		if (it->second) ileNiezerowych++;
	}
	unsigned int i = 0;
	for (auto it = s.mapa.begin(); it != s.mapa.end(); it++) {
		if (it->second) {
			wyjscie << it->first.index;
			wyjscie << " -> ";
			wyjscie << it->second;
			if (i + 1 != ileNiezerowych) {
				wyjscie << ", ";
			}
			i++;
		}
	}
	wyjscie << ")";
	return wyjscie;
}

const QuaternionSequence QuaternionSequence::
operator*(const Quaternion &other) const {
	QuaternionSequence next = *this;
	next *= other;
	return next;
}

QuaternionSequence operator*(const Quaternion& a, const QuaternionSequence& b) {
	QuaternionSequence next = b;
	next *= a;
	return next;
}

QuaternionSequence operator*(const double& a, const QuaternionSequence& b) {
	return Quaternion(a)*b;
}

bool QuaternionSequence::operator==(const QuaternionSequence& dane) const {
	for (auto it = mapa.begin(); it != mapa.end(); it++) {
		auto pos = dane.mapa.find(it->first);
		if (it->second) {
			if (pos == dane.mapa.end()) {
				return false;
			} else {
				if (pos->second != it->second) {
					return false;
				}
			}
		} else {
			if (pos != dane.mapa.end()) {
				if (pos->second != it->second) {
					return false;
				}
			}
		}
	}
	return true;
}

bool QuaternionSequence::operator!=(const QuaternionSequence& dane) const {
	return !(*this == dane);
}

QuaternionSequence::operator bool() const {
	for (auto it = mapa.begin(); it != mapa.end(); it++) {
		if (it->second) {
			return true;
		}
	}
	return false;
}

bool QuaternionSequence::operator!() const {
	if (*this) {
		return false;
	} else {
		return true;
	}
}

int QuaternionSequence::count() {
	return numberOfActiveSequences;
}

void QuaternionSequence::insert(long long n, Quaternion q) {
	QuaternionSequence::size_type ind(n);
	if (mapa.find(ind) != mapa.end()) {
		mapa.erase(mapa.find(ind));
	}
	mapa.insert(make_pair(ind, q));
}

Quaternion QuaternionSequence::operator[](long long i) {
	QuaternionSequence::size_type type(i);
	if (mapa.find(type) == mapa.end()) {
		return Quaternion();
	}
	return mapa.find(type)->second;
}


QuaternionSequence& QuaternionSequence::operator*=(const double &a) {
	return (*this)*=Quaternion(a);
}

const QuaternionSequence QuaternionSequence::
operator*(const double &a) const {
	return (*this)*Quaternion(a);
}
