#ifndef QUATERNION_H
#define	QUATERNION_H
#include <iostream>
using std::ostream;
//CALA IMPLEMENTACJA QUATERNION W QUATERNION.H
class Quaternion {
public:
	Quaternion();
	Quaternion(double r);
	Quaternion(double re, double im);
	Quaternion(double r, double i, double j, double k);
	Quaternion(const Quaternion& base);
	Quaternion(Quaternion&& base);
	Quaternion conj() const;
	double norm() const;
	double R() const;
	double I() const;
	double J() const;
	double K() const;
	bool operator==(const Quaternion & dane) const;
	bool operator!=(const Quaternion & dane) const;
	bool operator==(const double& dane) const;
	bool operator!=(const double& dane) const;
	const Quaternion operator-() const;
	const Quaternion operator+() const;
	Quaternion& operator=(const Quaternion &base);
	Quaternion& operator=(Quaternion&& base);
	Quaternion& operator+=(const Quaternion &rhs);
	Quaternion& operator-=(const Quaternion &rhs);
	Quaternion& operator*=(const Quaternion &rhs);
	Quaternion& operator*=(const double &q);
	friend ostream & operator<<(ostream &wyjscie, const Quaternion &s);
	operator bool() const;
	bool operator!() const;
	const Quaternion operator+(const Quaternion &other) const;
	const Quaternion operator-(const Quaternion &other) const;
	const Quaternion operator*(const Quaternion &other) const;
	/*	USUNAC NIEPOTRZEBNE OPERATORY
		OPERATORY DWUARGUMENTOWE OUTSIDE
	const Quaternion operator*(const double &other) const;
	const Quaternion operator+(const double &other) const;
	*/	
	Quaternion& operator+=(const double &q);
	const Quaternion operator-(const double &other) const;
	Quaternion& operator-=(const double &q);

private:
	double IndexR;
	double IndexI;
	double IndexJ;
	double IndexK;
	operator double() const;
	operator int() const;
};
Quaternion conj(Quaternion base);
double norm(Quaternion base);
static const Quaternion I(0, 1, 0, 0);
static const Quaternion J(0, 0, 1, 0);
static const Quaternion K(0, 0, 0, 1);
Quaternion operator*(const double& a, const Quaternion& b);
Quaternion operator+(const double& a, const Quaternion& b);
Quaternion operator-(const double& a, const Quaternion& b);
bool operator==(const double& a, const Quaternion& b);
bool operator!=(const double& a, const Quaternion& b);
#endif
